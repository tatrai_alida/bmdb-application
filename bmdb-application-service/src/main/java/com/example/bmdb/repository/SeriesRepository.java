package com.example.bmdb.repository;

import org.springframework.stereotype.Repository;

@Repository
public interface SeriesRepository extends MediaRepository {
}