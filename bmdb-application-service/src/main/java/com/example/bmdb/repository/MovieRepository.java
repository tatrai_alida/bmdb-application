package com.example.bmdb.repository;

import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends MediaRepository {
}
