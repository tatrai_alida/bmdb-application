package com.example.bmdb.service;

import com.example.bmdb.domain.*;
import com.example.bmdb.repository.MediaRepository;
import com.example.bmdb.repository.ReviewRepository;
import com.example.bmdb.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;


public class Service {

    private static final Logger logger = LoggerFactory.getLogger(Service.class);

    @Autowired
    UserRepository userRepository;
    @Autowired
    ReviewRepository reviewRepository;
    @Autowired
    MediaRepository mediaRepository;

    private User user;
    private List<Media> medias;
    private List<Review> reviews;

    public Service() {
        this.reviews = new ArrayList<>();
        this.medias = new ArrayList<>();
    }

    public void saveUser(User user) {
        this.user = user;
        userRepository.save(user);
    }


    public User findUser() {
        Iterable<User> users = this.userRepository.findAll();
        return users.iterator().hasNext() ? users.iterator().next() : null;
    }


    public List<Media> findAllMedia() {
        List<Media> medias = new ArrayList<>();
        Iterable<Media> iterable = mediaRepository.findAll();
        iterable.forEach(medias::add);
        return medias;
    }

    public Optional<Media> findMedia(Integer id) {

        Optional<Media> media = mediaRepository.findById(id);

        return media;
    }


    public void saveReview(Media media, Review review) {
        review.setMedia(media);
        media.getReviews().add(review);
        mediaRepository.save(media);
    }

    public List<Review> findAllReview(Media media) {
        Iterable<Review> iterable = reviewRepository.findAll();
        iterable.iterator().forEachRemaining(reviews::add);
        return reviews;
    }

    public double calculateAverageRating(Media media) {
        int sum = 0;
        int count = 0;
        for (Review review : findAllReview(media)) {
            sum = sum + review.getRating().rate;
            count = count + 1;
        }

        double avg = sum/count;

        return avg;

    }

    public void generateData(){
        mediaRepository.saveAll(generateTestData());
    }


    public static List<Media> generateTestData() {
        User user1 = new User.Builder()
                .withName("Béla")
                .withEmail("bela@test.com")
                .withPassword("kutya")
                .build();
        User user2 = new User.Builder()
                .withName("Karesz")
                .withEmail("karesz@test.com")
                .withPassword("kutya")
                .build();
        User user3 = new User.Builder()
                .withName("Julcsi")
                .withEmail("julcsi@test.com")
                .withPassword("kutya")
                .build();

        Actor JessieBuckley = new Actor.Builder()
                .withName("Jessie Buckley")
                .withBornDate(LocalDate.of(1989,12,28))
                .build();
        Actor JaredHarris = new Actor.Builder()
                .withName("Jared Harris")
                .withBornDate(LocalDate.of(1961,8,24))
                .build();
        Actor StellanSkarsgard = new Actor.Builder()
                .withName("Stellan Skarsgård")
                .withBornDate(LocalDate.of(1951,6,13))
                .build();
        Actor DaisyRidley = new Actor.Builder()
                .withName("Daisy Ridley")
                .withBornDate(LocalDate.of(1992,4,10))
                .build();
        Actor AdamDriver = new Actor.Builder()
                .withName("Adam Driver")
                .withBornDate(LocalDate.of(1983,11,19))
                .build();
        Actor MarkHamill = new Actor.Builder()
                .withName("Mark Hamill")
                .withBornDate(LocalDate.of(1951,9,25))
                .build();
        Actor MattDamon = new Actor.Builder()
                .withName("Matt Damon")
                .withBornDate(LocalDate.of(1970,10,8))
                .build();
        Actor ChristianBale = new Actor.Builder()
                .withName("Christian Bale")
                .withBornDate(LocalDate.of(1974,1,30))
                .build();
        Actor JonBerthal = new Actor.Builder()
                .withName("Jon Bernthal")
                .withBornDate(LocalDate.of(1976,9,20))
                .build();
        Actor LeonardiDiCaprio = new Actor.Builder()
                .withName("Leonardo DiCaprio")
                .withBornDate(LocalDate.of(1974,11,11))
                .build();
        Actor JosephGordonLevitt = new Actor.Builder()
                .withName("Joseph Gordon-Levitt")
                .withBornDate(LocalDate.of(1981,1,30))
                .build();
        Actor EllenPage = new Actor.Builder()
                .withName("Ellen Page")
                .withBornDate(LocalDate.of(1987,2,20))
                .build();

        Media Chernobil = new Media.Builder()
                .withId(new BigDecimal(1))
                .withTitle("Chernobyl")
                .withDescription("In April 1986, an explosion at the Chernobyl nuclear power plant in the Union of Soviet Socialist Republics becomes one of the world's worst man-made catastrophes.")
                .withPremier(LocalDate.of(2019,5,7))
                .withCast(Arrays.asList(JessieBuckley,JaredHarris,StellanSkarsgard))
                .build();
        Media Starwars = new Media.Builder()
                .withId(new BigDecimal(2))
                .withTitle("Star Wars: Episode IX - The Rise of Skywalker")
                .withDescription("The surviving members of the resistance face the First Order once again, and the legendary conflict between the Jedi and the Sith reaches its peak bringing the Skywalker saga to its end.")
                .withPremier(LocalDate.of(2019,12,19))
                .withCast(Arrays.asList(DaisyRidley,AdamDriver,MarkHamill))
                .build();
        Media FordVFerrari = new Media.Builder()
                .withId(new BigDecimal(3))
                .withTitle("Ford v Ferrari")
                .withDescription("'American car designer Carroll Shelby and driver Ken Miles battle corporate interference, the laws of physics and their own personal demons to build a revolutionary race car for Ford and challenge Ferrari at the 24 Hours of Le Mans in 1966.")
                .withPremier(LocalDate.of(2019,11,15))
                .withCast(Arrays.asList(MattDamon,ChristianBale,JonBerthal))
                .build();
        Media Inception = new Media.Builder()
                .withId(new BigDecimal(4))
                .withTitle("Inception")
                .withDescription("A thief who steals corporate secrets through the use of dream-sharing technology is given the inverse task of planting an idea into the mind of a C.E.O.")
                .withPremier(LocalDate.of(2010,7,16))
                .withCast(Arrays.asList(LeonardiDiCaprio,JosephGordonLevitt,EllenPage))
                .build();
        Media Egyszervolt = new Media.Builder()
                .withId(new BigDecimal(5))
                .withTitle("Egyszervolt")
                .withDescription("Kellett egy ötödik film is...")
                .withPremier(LocalDate.of(2010,7,16))
                .withCast(Arrays.asList(LeonardiDiCaprio,JosephGordonLevitt,EllenPage))
                .build();

        Review chernobilRev1 = new Review.Builder()
                .withCreator(user1)
                .withRating(Rating.GOOD)
                .withMedia(Chernobil)
                .withText("Excellent series")
                .build();
        Review chernobilRev2 = new Review.Builder()
                .withCreator(user2)
                .withRating(Rating.AVERAGE)
                .withMedia(Chernobil)
                .withText("I don't understand the hype...")
                .build();
        Review starwarsRev1 = new Review.Builder()
                .withCreator(user3)
                .withRating(Rating.AVERAGE)
                .withMedia(Starwars)
                .withText("I don't understand the hype...")
                .build();
        Review starwarsRev2 = new Review.Builder()
                .withCreator(user2)
                .withRating(Rating.GOOD)
                .withMedia(Starwars)
                .withText("Excellent movie")
                .build();
        Review fordVferrariRev1 = new Review.Builder()
                .withCreator(user3)
                .withRating(Rating.AVERAGE)
                .withMedia(FordVFerrari)
                .withText("well done for its genre, but only just, good acting on the part of Bale and Damon")
                .build();
        Review fordVferrariRev2 = new Review.Builder()
                .withCreator(user3)
                .withRating(Rating.GOOD)
                .withMedia(FordVFerrari)
                .withText("A lot of notes were hit by Ford v Ferrari. The characters are fleshed out very well and give you the emotional attachment you're looking for in a movie.")
                .build();
        Review inceptionRev1 = new Review.Builder()
                .withCreator(user2)
                .withRating(Rating.AVERAGE)
                .withMedia(Inception)
                .withText("I cannot understand why this movie is rated so highly, it is infantile in its storyline and soooo boring.")
                .build();
        Review inceptionRev2 = new Review.Builder()
                .withCreator(user3)
                .withRating(Rating.GOOD)
                .withMedia(Inception)
                .withText("'Surrealism can appear to be ineffably bizarre, or inquisitively titillating, depending purely on the viewer's intellect.")
                .build();
        Review inceptionRev3 = new Review.Builder()
                .withCreator(user1)
                .withRating(Rating.GOOD)
                .withMedia(Inception)
                .withText("An teresting movie.")
                .build();

        JessieBuckley.setFilmography(Collections.singletonList(Chernobil));
        JaredHarris.setFilmography(Collections.singletonList(Chernobil));
        StellanSkarsgard.setFilmography(Collections.singletonList(Chernobil));
        DaisyRidley.setFilmography(Collections.singletonList(Starwars));
        AdamDriver.setFilmography(Collections.singletonList(Starwars));
        MarkHamill .setFilmography(Collections.singletonList(Starwars));
        MattDamon.setFilmography(Collections.singletonList(FordVFerrari));
        ChristianBale.setFilmography(Collections.singletonList(FordVFerrari));
        JonBerthal.setFilmography(Collections.singletonList(FordVFerrari));
        LeonardiDiCaprio.setFilmography(Collections.singletonList(Inception));
        JosephGordonLevitt.setFilmography(Collections.singletonList(Inception));
        EllenPage.setFilmography(Collections.singletonList(Inception));

        Chernobil.setReviews(new ArrayList<>(Arrays.asList(chernobilRev1,chernobilRev2)));
        Starwars.setReviews(new ArrayList<>(Arrays.asList(starwarsRev1,starwarsRev2)));
        FordVFerrari.setReviews(new ArrayList<>(Arrays.asList(fordVferrariRev1,fordVferrariRev2)));
        Inception.setReviews(new ArrayList<>(Arrays.asList(inceptionRev1,inceptionRev2,inceptionRev3)));
        Egyszervolt.setReviews(new ArrayList<>());

        return Arrays.asList(Chernobil,Starwars,FordVFerrari,Inception,Egyszervolt);
    }
}
