package com.example.bmdb.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Media {
    @Id
    @GeneratedValue
    private BigDecimal id;
    private String title;
    private String description;
    private LocalDate premier;
    @ManyToMany(cascade = CascadeType.ALL)
    private List<Actor> cast;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "media", fetch = FetchType.LAZY)
    private List<Review> reviews;

    public Media() {
    }

    public Media(BigDecimal id, String title, String description, LocalDate premier, List<Actor> cast, List<Review> reviews) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.premier = premier;
        this.cast = cast;
        this.reviews = reviews;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getPremier() {
        return premier;
    }

    public void setPremier(LocalDate premier) {
        this.premier = premier;
    }

    public List<Actor> getCast() {
        return cast;
    }

    public void setCast(List<Actor> cast) {
        this.cast = cast;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public static class Builder {
        private BigDecimal id;
        private String title;
        private String description;
        private LocalDate premier;
        private List<Actor> cast = new ArrayList<>();
        private List<Review> reviews = new ArrayList<>();

        public Builder() {
        }

        public Media.Builder withId(BigDecimal id)
        {
            this.id = id;
            return  this;
        }

        public Media.Builder withTitle(String title){
            this.title= title;
            return this;
        }

        public Media.Builder withDescription(String description){
            this.description= description;
            return this;
        }

        public Media.Builder withPremier(LocalDate premier){
            this.premier= premier;
            return this;
        }

        public Media.Builder withCast(List<Actor> cast){
            this.cast= cast;
            return this;
        }

        public Media.Builder withReviews(List<Review> reviews){
            this.reviews= reviews;
            return this;
        }

        public Media build() {return new Media(this.id, this.title, this.description, this.premier, this.cast, this.reviews);}

    }
}
