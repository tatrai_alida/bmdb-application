package com.example.bmdb.domain;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Actor {

    @Id
    private String name;
    private LocalDate born;
    private String biography;
    @ManyToMany(cascade = CascadeType.ALL)
    private List<Media> filmography;
    private Sex sex;

    public Actor() {
    }

    public Actor(String name, LocalDate born, String biography, List<Media> filmography, Sex sex) {
        this.name = name;
        this.born = born;
        this.biography = biography;
        this.filmography = filmography;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBorn() {
        return born;
    }

    public void setBorn(LocalDate born) {
        this.born = born;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public List<Media> getFilmography() {
        return filmography;
    }

    public void setFilmography(List<Media> filmography) {
        this.filmography = filmography;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public static class Builder {
        private String name;
        private LocalDate born;
        private String biography;
        private List<Media> filmography = new ArrayList();
        private Sex sex;

        public Builder() {
        }

        public Actor.Builder withName(String name)
        {
            this.name = name;
            return  this;
        }

        public Actor.Builder withBornDate(LocalDate born){
            this.born= born;
            return this;
        }

        public Actor.Builder withBiography(String biography){
            this.biography= biography;
            return this;
        }

        public Actor.Builder withFilmography(List<Media> filmography){
            this.filmography= filmography;
            return this;
        }

        public Actor.Builder withSex(Sex sex){
            this.sex= sex;
            return this;
        }

        public Actor build() {return new Actor(this.name, this.born, this.biography, this.filmography, this.sex);}

    }
}
