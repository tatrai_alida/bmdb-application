package com.example.bmdb.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class User {
    @Id
    private String name;
    private String email;
    private String passWord;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "creator", fetch=FetchType.LAZY)
    private List<Review> reviews = new ArrayList();

    public User() {
    }

    public User(String name, String email, String passWord, List<Review> reviews) {
        this.name = name;
        this.email = email;
        this.passWord = passWord;
        this.reviews = reviews;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public static class Builder {
        private String name;
        private String email;
        private String passWord;
        private List<Review> reviews= new ArrayList<>();

        public Builder() {
        }

        public User.Builder withName(String name)
        {
            this.name = name;
            return this;
        }

        public User.Builder withEmail(String email){
            this.email= email;
            return this;
        }

        public User.Builder withPassword(String passWord){
            this.passWord= passWord;
            return this;
        }

        public User.Builder withReviews(List<Review> reviews){
            this.reviews= reviews;
            return this;
        }


        public User build() {return new User(this.name, this.email, this.passWord, this.reviews);}

    }
}
