package com.example.bmdb.domain;

import javax.persistence.*;

@Entity
public class Review {

    @Id
    @GeneratedValue
    private int id;

    private String text;
    @Enumerated(EnumType.STRING)
    private Rating rating;
    @ManyToOne(cascade = CascadeType.ALL)
    private User creator;
    @ManyToOne(cascade = CascadeType.ALL)
    private Media media;

    public Review() {
    }

    public Review(String text, Rating rating, User creator, Media media) {
        this.text = text;
        this.rating = rating;
        this.creator = creator;
        this.media = media;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    public static class Builder {
        private String text;
        private Rating rating;
        private User creator;
        private Media media;

        public Builder() {
        }

        public Review.Builder withText(String text)
        {
            this.text = text;
            return this;
        }

        public Review.Builder withRating(Rating rating){
            this.rating= rating;
            return this;
        }

        public Review.Builder withCreator(User creator){
            this.creator= creator;
            return this;
        }

        public Review.Builder withMedia(Media media){
            this.media= media;
            return this;
        }


        public Review build() {return new Review(this.text, this.rating, this.creator, this.media);}

    }
}
