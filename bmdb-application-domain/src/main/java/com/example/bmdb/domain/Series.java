package com.example.bmdb.domain;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Entity
public class Series extends Media {
    public Series(BigDecimal id, String title, String description, LocalDate premier, List<Actor> cast, List<Review> reviews){
        super(id, title, description, premier, cast, reviews);
    }
}
