package com.example.bmdb.config;

import com.example.bmdb.app.App;
import com.example.bmdb.service.Service;
import com.example.bmdb.view.View;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ResourceBundleMessageSource;

@Configuration
@Import(RepositoryConfig.class)
public class AppConfig {

    @Bean
    public App app(Service service, View view){
        return new App(service, view);
    }

    @Bean
    public Service service(){
        return new Service();
    }

    @Bean
    public View view(){
        return new View();
    }

    @Bean
    public ResourceBundleMessageSource messageSource() {

        ResourceBundleMessageSource source = new ResourceBundleMessageSource();
        source.setBasenames("messages/strings");
        source.setUseCodeAsDefaultMessage(true);

        return source;
    }





}
