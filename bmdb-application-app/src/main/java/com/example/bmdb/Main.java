package com.example.bmdb;

import com.example.bmdb.app.App;
import com.example.bmdb.config.AppConfig;
import com.example.bmdb.repository.MediaRepository;
import com.example.bmdb.service.Service;
import com.example.bmdb.view.View;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) {
        try (ConfigurableApplicationContext appContext = new AnnotationConfigApplicationContext(AppConfig.class)) {
            App app = appContext.getBean(App.class);
            app.play();
        }
    }
}
