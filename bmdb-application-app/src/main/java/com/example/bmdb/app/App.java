package com.example.bmdb.app;

import com.example.bmdb.domain.*;
import com.example.bmdb.service.*;
import com.example.bmdb.view.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

@Component
@Transactional
public class App {

    private static final Logger logger = LoggerFactory.getLogger(App.class);

    @Autowired
    private MessageSource messageSource;
    Review review;
    Media selectedMedia;
    List<Media> medias;
    Service service;
    View view;
    User currentUser;

    public App(Service service, View view) {
        this.service = service;
        this.view = view;
    }



    public void play(){
        this.service.generateData();
        this.createUser();
        this.doReview();
    }

    public void createUser(){
        this.currentUser= this.view.readUserData();
        this.service.saveUser(this.currentUser);

    }

    public void doReview(){
        List<Media> medias = this.service.findAllMedia();
        for (Media media : medias){
            media.getCast().size();
            media.getReviews().size();
        }
        this.view.printMedias(this.service.findAllMedia());
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println( messageSource.getMessage("chooseMedia", null, Locale.getDefault()) + "\n");
            BigDecimal id = new BigDecimal(reader.readLine());
            selectedMedia = medias.stream().filter(m -> m.getId().equals(id)).findFirst().get();
            System.out.println(messageSource.getMessage("writeReview", null, Locale.getDefault())+"\n");
            String review = reader.readLine();
            System.out.println(messageSource.getMessage("chooseRating", null, Locale.getDefault())+"\n");
            String rating = reader.readLine();

            this.service.saveReview(this.selectedMedia, new Review.Builder().withMedia(this.selectedMedia).withText(review).withRating(Rating.valueOf(rating)).withCreator(this.currentUser).build());

            this.printAverageRating();
            System.out.println(messageSource.getMessage("reviewAgain", null, Locale.getDefault()));
            String yesno = reader.readLine();
            if(yesno.equals(messageSource.getMessage("review.yes", null, Locale.getDefault())))
            {
                this.doReview();
            }

        } catch (IOException e) {

        }


    }

    public void printAverageRating(){
        this.view.printReviewAverage(this.service.calculateAverageRating(this.selectedMedia));
    }


}
