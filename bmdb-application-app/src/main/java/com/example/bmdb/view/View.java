package com.example.bmdb.view;

import com.example.bmdb.domain.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Locale;

@Component
@Transactional
public class View {

    private static final Logger logger = LoggerFactory.getLogger(View.class);

    @Autowired
    private MessageSource messageSource;

    public User readUserData() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println(messageSource.getMessage("readUserAskName", null, Locale.getDefault()));
            String name = reader.readLine();
            User user = new User.Builder().withName(name).build();
            this.printWelcomeMessage(user);

            return user;


        } catch (IOException e) {
            return null;
        }
    }

    public void printWelcomeMessage(User user) {
        System.out.println(messageSource.getMessage("welcome", null, Locale.getDefault()));
    }

    public void printMedias(List<Media> medias) {
        for (Media media : medias) {

            System.out.println((media.getId()) + ": " +
                    media.getTitle()+ "\n" +
                    messageSource.getMessage("field.description", null, Locale.getDefault()) + media.getDescription() + "\n" +
                    messageSource.getMessage("field.premier", null, Locale.getDefault()) + media.getPremier() + "\n " +
                    messageSource.getMessage("field.cast", null, Locale.getDefault()) + "\n" +
                    "["
            );

            for(Actor actor : media.getCast())
            {
                System.out.println(
                        messageSource.getMessage("field.actorName", null, Locale.getDefault()) + actor.getName() + ", " +messageSource.getMessage("field.actorBorn", null, Locale.getDefault()) + actor.getBorn() + ",\n"
                );

            }

            System.out.println(
                    "]\n"
            );

            for(Review review : media.getReviews() )
            {
                System.out.println( media.getReviews().indexOf(review) + ": " + review.getText() + "\n" + messageSource.getMessage("field.review", null, Locale.getDefault())+
                        "=" + review.getRating()

                );
            }

            System.out.println(
                    "]\n"
            );

        }

    }

    public void printReviews(User user) {
        System.out.println(
                "]\n" + messageSource.getMessage("field.reviews", null, Locale.getDefault())+": [ "
        );

        for(Review review : user.getReviews() )
        {
            System.out.println( user.getReviews().indexOf(review) + ": " + review.getText() + "\n" + messageSource.getMessage("field.review", null, Locale.getDefault())+
                    "=" + review.getRating()

            );
        }


    }


    public void printReviewAverage(double avg) {
        System.out.println(
                "\n" + messageSource.getMessage("averageRating", null, Locale.getDefault()) + avg
        );

    }
}
