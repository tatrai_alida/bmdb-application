package com.example.bmdb.config;

import com.example.bmdb.service.Service;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
@Configuration
public class WebConfig{

    @Bean
    public Service service(){
        return new Service();
    }

}
