package com.example.bmdb.controllers;

import com.example.bmdb.domain.User;
import com.example.bmdb.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.Optional;

@Controller
@Transactional
public class LoginController {


    @Autowired
    Service service;


    @RequestMapping(path = "/login", method = RequestMethod.GET)
    public String loginView(HttpSession session){
        if(session.getAttribute("user") != null){
            return "redirect:/home";
        }
        return "login";
    }

    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public String login(@RequestParam("username") String username,
                        @RequestParam("password") String password,
                        HttpSession session,
                        ModelMap modelMap){

        Optional<User> userLookup = Optional.ofNullable(this.service.findUser());

        User user;

        if(userLookup.isPresent()){
            user = userLookup.get();
        }else {
            modelMap.put("error", "User not registered");
            return "redirect:/login";
        }

        if(username.equalsIgnoreCase(user.getEmail()) && password.equalsIgnoreCase(user.getPassWord())) {
            session.setAttribute("user", user);
            return "redirect:/home";
        } else {
            modelMap.put("error", "Invalid credentials");
            return "redirect:/login";
        }
    }

    @RequestMapping(path = "/logout", method = RequestMethod.POST)
    public String logout(HttpSession session){
        session.removeAttribute("user");
        return "redirect:/login";
    }
}
