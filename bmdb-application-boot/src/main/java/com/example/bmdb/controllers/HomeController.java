package com.example.bmdb.controllers;


import com.example.bmdb.domain.Media;
import com.example.bmdb.domain.Review;
import com.example.bmdb.domain.User;
import com.example.bmdb.repository.MediaRepository;
import com.example.bmdb.repository.UserRepository;
import com.example.bmdb.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@Transactional
public class HomeController {

    @Autowired
    Service service;

    @Autowired
    MediaRepository mediaRepository;


    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String homepage(HttpSession session, ModelMap modelMap) {


        User user = (User) session.getAttribute("user");
        if (user == null) {
            return "redirect:/login";
        }

        List<Media> medias = service.findAllMedia();

        List<Review> reviews = medias.stream()
                .flatMap(m -> m.getReviews().stream())
                .collect(Collectors.toList());

        modelMap.addAttribute("user", user);
        modelMap.addAttribute("reviews", reviews);

        return "home";
    }

    @RequestMapping(value = {"/init"}, method = RequestMethod.GET)
    public String initApplication() {

        mediaRepository.saveAll(Service.generateTestData());

        return "redirect:/home";
    }
}