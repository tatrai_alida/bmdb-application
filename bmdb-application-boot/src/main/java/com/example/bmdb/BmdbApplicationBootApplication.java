package com.example.bmdb;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BmdbApplicationBootApplication {

        public static void main(String[] args) {
            SpringApplication.run(BmdbApplicationBootApplication.class, args);
        }

}
